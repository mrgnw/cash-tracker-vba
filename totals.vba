Sub copy_totals()
	Sheets("Goats").Select
	Range("E3:E8").Select
	Selection.Copy
	Range("B3:B8").Select
	Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    	:=False, Transpose:=False
	
	Range("E10:E15").Select
	Application.CutCopyMode = False
	Selection.Copy
	Range("B10").Select
	Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    	:=False, Transpose:=False
	Application.CutCopyMode = False
	
	ActiveWorkbook.Save
End Sub



Sub Goats()
' Ctrl+g selects Goats worksheet.
	Sheets("Goats").Select
	Cells(1, 1).Select
	'This is also a convenient time to save.
	ActiveWorkbook.Save
End Sub



Sub clear_mid()
	ActiveWindow.SmallScroll Down:=-9
	Range("C3:C8").Select
	Selection.ClearContents
	Range("C10:C13").Select
	Selection.ClearContents
	Call SelectFirstBlankCell
End Sub


Sub clear_end()
	Range("D3:D8").Select
	Selection.ClearContents
	Range("D10:D13").Select
	Selection.ClearContents
	Call SelectFirstBlankCell
End Sub