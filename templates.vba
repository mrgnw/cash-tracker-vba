Sub apply_template()
	curSheet = ActiveSheet.Name
	'if in quick, paste_template
	If curSheet = "Quick" Then
    	Call paste_template
	
	'Else: (go to cash), repeat_template
	Else:
    	Call go_to_cash
    	Call paste_template ' I want 1 template to be standard
    	'Call repeat_template
    	
	End If



Sub paste_template()
	prev = ActiveSheet.Name
	Sheets("Totes").Select
	Range("A3:A8").Select
	Selection.Copy
	Sheets(prev).Select
	Call SelectFirstBlankCell
	
	startRow = ActiveCell.Row
	
	Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    	:=False, Transpose:=False
	Range("B2").Select
	Application.CutCopyMode = False
	
	' Select Row B first in template
	Cells(startRow, 2).Select
	
End Sub



Sub repeat_template()
	Call SelectFirstBlankCell
	currentRow = ActiveCell.Row
	
	'Cells(currentRow, 1) = currentRow
	
	
	maxRow = 200
	currentRow = ActiveCell.Row
	' fill to maxRow
	'Do While currentRow < maxRow
	'	Call SelectFirstBlankCell
	'	currentRow = ActiveCell.Row
    	' MsgBox currentRow
	'	Call paste_template
	'Loop
	
End Sub



'Sub repeat_template_times(Optional x_times As Integer = 5)
 '   Call SelectFirstBlankCell
  '  currentRow = ActiveCell.Row
   '
   ' For Count = 1 To x_times
   ' 	Call SelectFirstBlankCell
	'	currentRow = ActiveCell.Row
    	' MsgBox currentRow
	'	Call paste_template
	'Next
' End Sub