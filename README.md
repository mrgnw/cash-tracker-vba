Cash tracker is normally an ordinary spreadsheet that tellers can use as a second way to track their cash flow.

When I was a teller, I created my own cash tracker with VBA to optimize the process.

Instead of a huge form with pre-set values, this cash tracker allows tellers to enter precisely what they're working with. This improves readability and flexibility. Additionally, macros with keyboard shortcuts streamline the process.


Cash tracker VBA
=====

cash.xlsm is an Excel document that is used to help tellers prevent outages & overages.
Though financial institutions have systems that track cash flow on their own, tellers can also use a secondary tool like cash.xlsm to help prevent outages & overages.

As programs go, cash is relatively simple. Cash is a very useful tool that has more features than simply counting cash.

Features:
------
###Keyboard shortcuts.
###Autosave 
###Clear
###Add template
> Instead of having dozens of pre-filled forms with values that won't even be used, we just add them as we go.
> However, if you need to enter a number of standard values (20's, 10's, 5's, 1's), press Ctrl+T to automatically load all of those into the first column.
> Each time Ctrl+T is pressed, a new set of values is added to the next available space.
  
### Highlight denominations  
Off by $20? Highlight all fields where $20 bills were used.



> The vba code from cash.xlsm has been split into vba files to provide a structure that's easy to understand.
> The code actually used is packaged within the .xlsm file.


