Public Sub SelectFirstBlankCell()
	Dim sourceCol As Integer, rowCount As Integer, currentRow As Integer
	Dim currentRowValue As String
	sourceCol = 1   'column A has a value of 1
	'rowCount = 50
	rowCount = Cells(Rows.Count, sourceCol).End(xlUp).Row
	'for every row, find the first blank cell and select it
	row_num = 1
	
	For currentRow = 2 To rowCount
    	currentRowValue = Cells(currentRow, sourceCol).Value
    	If IsEmpty(currentRowValue) Or currentRowValue = "" Then
        	' Cells(currentRow, sourceCol).Select
    	Else
        	row_num = currentRow
    	End If
	Next
	
	row_num = row_num + 2
	
	If row_num = 3 Then
    	row_num = 2
    	End If
	
	Cells(row_num, 1).Select
	
	If ActiveSheet.Name = "Goats" Then
    	Cells(2, 1).Select
    	Cells(1, 1).Select
    	End If
End Sub