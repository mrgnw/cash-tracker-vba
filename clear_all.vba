Sub clear_all()
' Clears all manually-entered values in Cash & Quick
	' Confirm clear
	Answer = MsgBox("Start over?", vbQuestion + vbYesNo, "Clear all?")
	
	‘modified from negative if vbNo
	If Answer = vbYes Then
    	ActiveWorkbook.Save
    		' Grab current worksheet (to return to at end)
    	prev = ActiveSheet.Name
    	
    	' Copy end of day totals to start of day
    	Sheets("Goats").Select
    	Range("E3:E8").Select
    	Selection.Copy
    	Range("B3:B8").Select
    	Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        	:=False, Transpose:=False
    	' Coin, bait, mutilated
    	Application.CutCopyMode = False
    	Range("E10:E15").Select
    	Selection.Copy
    	Range("B10").Select
    	Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        	:=False, Transpose:=False
    	Application.CutCopyMode = False
    	
    	
    	' CLEAR CASH
    	Sheets("Cash").Select
    	Range("A2:C999").Select
    	Selection.ClearContents
    	Range("F2:G999").Select
    	Selection.ClearContents
    	
    	' CLEAR QUICK
    	Call clear_quick ' goes to quick
    	Call clear_quick ' clears quick
    	
    	' return to prev
    	Sheets(prev).Select
    	Call SelectFirstBlankCell
	End If
	
End Sub