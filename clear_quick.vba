Sub clear_quick()
' clear_quick Macro
' Keyboard Shortcut: Ctrl+q
	prev = ActiveSheet.Name
	Sheets("Quick").Select
	Call SelectFirstBlankCell
	' save before clearing
If prev = "Quick" Then
	ActiveWorkbook.Save
	Sheets("Quick").Select
	Range("A2:C499").Select
	Selection.ClearContents
	
	Range("F2:F499").Select
	Selection.ClearContents
	
	Call SelectFirstBlankCell
End If
End Sub